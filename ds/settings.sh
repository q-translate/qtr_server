APP=qtr_server/ds
DOMAIN="qtr.example.org"
IMAGE=$DOMAIN

### Uncomment if this installation is for development.
DEV=true

### Other domains.
DOMAINS="dev.$DOMAIN"

### Admin settings.
ADMIN_PASS=123456
ADMIN_EMAIL=admin@example.org

### Translation languages supported by the Q-Translate Server.
LANGUAGES='en fr de it sq'

### DB settings
DBHOST=mariadb
DBPORT=3306
DBNAME=${DOMAIN//./_}
DBUSER=${DOMAIN//./_}
DBPASS=${DOMAIN//./_}
