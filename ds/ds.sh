rename_function cmd_remove ds_cmd_remove

cmd_remove() {
    # remove the container, image etc.
    ds_cmd_remove
    rm -rf drush-cache/ qtr_server var-www/

    # remove the databases
    ds @$DBHOST exec mysql -e "
        drop database if exists $DBNAME;
        drop database if exists ${DBNAME}_data;
        drop database if exists ${DBNAME}_dev;
        drop user if exists $DBUSER;
    "
}
